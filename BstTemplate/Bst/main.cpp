#include <iostream>
#include "BSNode.h"

#define MAX_SIZE 15

int main()
{
	int intArr[MAX_SIZE] = { 5,20,2,6,0,10,100,500,200,30,50,70,10,13,12 };
	string strArr[MAX_SIZE] = { "LOL",
								"Fortnite",
								"Pubg",
								"Minecraft",
								"Ilan aefes",
								"Ben is good",
								"Rami hatich",
								"Emil (::)",
								"Potato",
								"French fries",
								"McDonalds",
								"Burger King",
								"Adel",
								"friends",
								"Gitlab" };

	for (int i = 0; i < MAX_SIZE; i++)
	{
		std::cout << intArr[i] << " ";
	}
	std::cout << std::endl;
	for (int i = 0; i < MAX_SIZE; i++)
	{
		std::cout << strArr[i] << " ";
	}

	BSNode<int>* intTree = new BSNode<int>(intArr[0]);
	for (int i = 1; i < MAX_SIZE; i++)
	{
		intTree->insert(intArr[i]);
	}

	BSNode<string>* strTree = new BSNode<string>(strArr[0]);
	for (int i = 1; i < MAX_SIZE; i++)
	{
		strTree->insert(strArr[i]);
	}

	std::cout << "int tree" << std::endl;
	intTree->printNodes();
	std::cout << "string tree" << std::endl;
	strTree->printNodes();

	system("pause");
	return 0;
}