#ifndef BSNode_H
#define BSNode_H

#include <iostream>
#include <string>

#define ERROR_NOT_IN_SUBTREE -1

using namespace std;

template <class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode<T>& other);

	~BSNode();
	
	void insert(T value);
	BSNode<T>& operator=(const BSNode<T>& other);

	bool isLeaf() const;
	T getData() const;
	BSNode<T>* getLeft() const;
	BSNode<T>* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode<T>& root) const;

	void printNodes() const; //for question 1 part C

private:
	T _data;
	BSNode<T>* _left;
	BSNode<T>* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode<T>* node) const; //auxiliary function for getDepth
	void freeTree();

};

template <class T>
BSNode<T>::BSNode(T data) : _data(data), _left(nullptr), _right(nullptr), _count(1)
{

}

template <class T>
BSNode<T>::BSNode(const BSNode<T>& other)
{
	_data = other._data;

	if (other._left)
	{
		_left = new BSNode<T>(*(other._left));
	}
	else
	{
		_left = nullptr;
	}

	if (other._right)
	{
		_right = new BSNode<T>(*(other._right));
	}
	else
	{
		_right = nullptr;
	}
}

template <class T>
BSNode<T>::~BSNode()
{
	freeTree();
}

/*
Inserts the val to the tree
*/
template <class T>
void BSNode<T>::insert(T value)
{
	if (_data < value) //checks if the value is bigger than the current node
	{
		if (_right)
		{
			_right->insert(value); //continues to the right
		}
		else
		{
			_right = new BSNode<T>(value); //if we came across the last node we create a new one to the right
		}
	}
	else if (_data > value) //checks if the value is smaller than the current node
	{
		if (_left)
		{
			_left->insert(value); //continues to the left
		}
		else
		{
			_left = new BSNode<T>(value); //if we came across the last node we create a new one to the left
		}
	}
	else
	{
		_count++;
	}
}

/*
Function overload for =
Deep Copy of a node
*/
template <class T>
BSNode<T>& BSNode<T>::operator=(const BSNode<T>& other)
{
	BSNode<T> temp(other._data);

	if (other._left)
	{
		temp._left = new BSNode<T>(*(other._left));
	}
	else
	{
		temp._left = nullptr;
	}

	if (other._right)
	{
		temp._right = new BSNode<T>(*(other._right));
	}
	else
	{
		temp._right = nullptr;
	}

	return temp;
}

//checks if leaf
template <class T>
bool BSNode<T>::isLeaf() const
{
	if (!_left && !_right)
	{
		return true;
	}
	else
	{
		return false;
	}
}

template <class T>
T BSNode<T>::getData() const
{
	return _data;
}

template <class T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return _left;
}

template <class T>
BSNode<T>* BSNode<T>::getRight() const
{
	return _right;
}

/*
Function that checks if a val is in the BT
Input: string val - value to check
*/
template <class T>
bool BSNode<T>::search(T val) const
{
	bool result = false;

	if (_data == val) //if in tree
	{
		return true;
	}
	else if (_data < val && _right) //if the val is bigger than the current node and its not a leaf
	{
		result = _right->search(val); //goes to the next node on the right
	}
	else if (_data > val && _left) //if the val is smaller than the current node and its not a leaf
	{
		result = _left->search(val); //goes to the next node on the left
	}
	else
	{
		return false; //if its a leaf and the val is not found, it means the val is not in the tree
	}

	return result;
}


/*
Calculates the height of the tree
*/
template <class T>
int BSNode<T>::getHeight() const
{
	int hLeft = 0, hRight = 0;
	if (_right)
	{
		hRight = _right->getHeight();
	}

	if (_left)
	{
		hLeft = _left->getHeight();
	}

	if (hLeft > hRight)
	{
		return hLeft + 1;
	}
	else
	{
		return hRight + 1;
	}

	return 0;
}

template <class T>
int BSNode<T>::getDepth(const BSNode<T>& root) const
{
	return root.getCurrNodeDistFromInputNode(this);
}

/*
Prints nodes in order
*/
template <class T>
void BSNode<T>::printNodes() const
{
	if (_left) //goes to the leftest node
	{
		_left->printNodes();

	}

	cout << _data << " " << _count << endl; //prints

	if (_right) //continues to the right
	{
		_right->printNodes();
	}

}

/*
Calculates the depth of a node
*/
template <class T>
int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode<T>* node) const
{
	int count = 1;
	int result = 0;
	if (_data == node->_data) // if its in the end node
	{
		return 0;
	}
	else if (_data != node->_data && !_right && !_left) //if we at the end and the data is note correct than the node is not in the sub tree
	{
		return ERROR_NOT_IN_SUBTREE;
	}
	else
	{
		if (_data < node->_data) //checks its bigger than current node 
		{
			if ((result = _right->getCurrNodeDistFromInputNode(node)) == ERROR_NOT_IN_SUBTREE) //checks if not we came across the error
			{
				return result;
			}
			else
			{
				count += result; //adds to the count of arcs
			}
		}
		else
		{
			if ((result = _left->getCurrNodeDistFromInputNode(node)) == ERROR_NOT_IN_SUBTREE) //checks if not we came across the error
			{
				return result;
			}
			else
			{
				count += result;
			}
		}
	}
	return count; //returns the arcs
}

/*
Frees the tree
Deletes from bottom to up
*/
template <class T>
void BSNode<T>::freeTree()
{
	if (_right)
	{
		_right->freeTree(); //goes to the rightest node 
		delete _right;
		_right = nullptr;
	}

	if (_left)
	{
		_left->freeTree(); //goes to the leftest node
		delete _left;
		_left = nullptr;
	}
}


#endif