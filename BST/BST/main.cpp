#pragma comment(lib, "printTreeToFile.lib")
#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"
using namespace std;


int main()
{

	BSNode* bs1 = new BSNode("7");
	bs1->insert("7");
	bs1->insert("friends");
	bs1->insert("friends");
	bs1->insert("3");
	bs1->insert("3");
	bs1->insert("Hello");
	bs1->insert("Hello");
	bs1->insert("Bye");
	bs1->insert("Bye");
	bs1->insert("Alon");
	bs1->insert("Alon");
	bs1->insert("GoodBye");
	bs1->insert("GoodBye");
	bs1->insert("How are you");
	bs1->insert("How are you");
	bs1->insert("C++ is the best");
	bs1->insert("C++ is the best");
	bs1->insert("Just Kidding ... :) is better");
	bs1->insert("Just Kidding ... :) is better");

	bs1->printNodes();

	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");

	BSNode bs2 = *bs1;

	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;
	
	bs2.printNodes();

	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;
	delete bs1;

	return 0;
}
