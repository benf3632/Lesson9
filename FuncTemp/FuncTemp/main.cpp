#include <iostream>
#include "functions.h"
#include "Int.h"


int main() {
	
//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;


	//check compare
	std::cout << "correct print is -1 1 0" << std::endl;
	std::cout << compare<char>('b', 'C') << std::endl;
	std::cout << compare<char>('h', 'l') << std::endl;
	std::cout << compare<char>('d', 'd') << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size_char = 5;
	char charArr[arr_size_char] = { 'b', 'A', 'd', 'g', 'f' };
	bubbleSort<char>(charArr, arr_size_char);
	for (int i = 0; i < arr_size_char; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, arr_size_char);
	std::cout << std::endl;

	Int a(20);
	Int b(50);
	Int c(200);
	Int d(0);

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<Int>(a, b) << std::endl;
	std::cout << compare<Int>(c, b) << std::endl;
	std::cout << compare<Int>(d, d) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size_Int = 4;
	Int intArr[arr_size_Int] = { a, c, b, d};
	bubbleSort<Int>(intArr, arr_size_Int);
	for (int i = 0; i < arr_size_Int; i++) {
		std::cout << intArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<Int>(intArr, arr_size_Int);
	std::cout << std::endl;

	system("pause");
	return 1;
}