#pragma once
#include <ostream>
using namespace std;
class Int
{
public:
	Int(int value);
	~Int();
	int _data;
	bool operator<(const Int& other) const;
	bool operator>(const Int& other) const;
	bool operator==(const Int& other) const;
	void operator=(const Int& other);
	friend ostream& operator<<(ostream& os, const Int& other);
};

