#pragma once

#include <iostream>

#define ATHANB -1 
#define EQUAL 0
#define BTHANA 1

template <typename T>
void printArray(T* arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}

template <typename T>
void bubbleSort(T* arr, int size)
{
	T temp = 0;
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size - 1; j++)
		{
			if (arr[j + 1] < arr[j])
			{
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

template <typename T>
int compare(T a, T b)
{
	if (a > b)
	{
		return ATHANB;
	}
	else if (a == b)
	{
		return EQUAL;
	}
	else
	{
		return BTHANA;
	}
}



