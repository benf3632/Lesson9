#include "Int.h"



Int::Int(int value=0) : _data(value)
{
}


Int::~Int()
{
}

bool Int::operator<(const Int & other) const
{
	if (_data < other._data)
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

bool Int::operator>(const Int & other) const
{
	if (_data > other._data)
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

bool Int::operator==(const Int & other) const
{
	if (_data == other._data)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Int::operator=(const Int& other)
{
	_data = other._data;
}


ostream & operator<<(ostream & os, const Int & other)
{
	os << other._data;
	return os;
}
